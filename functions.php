<?php


function tomjn_typekit_code() {
	?>
	<script type="text/javascript" src="//use.typekit.net/upi1wjh.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<?php
}

function tomjn_less_vars( $vars, $handle ) {
	// $handle is a reference to the handle used with wp_enqueue_style()
	$vars[ 'typekitfontstack' ] = '"prenton", sans-serif';
	return $vars;
}
